---
layout: report
year: "2019"
month: "09"
month_name: "September"
title: "Reproducible builds in September 2019"
draft: true
---

* [939546 https://github.com/libguestfs/libnbd/pull/2](forwarded)

* [939549 https://github.com/sdaps/sdaps/pull/182](forwarded)

* [939547 https://github.com/sbabic/libubootenv/pull/3](forwarded)

* [FIXME](https://www.redhat.com/archives/libguestfs/2019-September/msg00037.html)
